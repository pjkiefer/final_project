describe("validation tests", function() {
    let badYearDate = '201-02-02';
    let goodDate = '2019-03-25';
    let badDataElement = document.createElement('input');
    badDataElement.type = 'text';
    badDataElement.value = badYearDate;

    let goodDataElement = document.createElement('input');
    goodDataElement.type = 'text';
    goodDataElement.value = goodDate;
    
    let parentElement = document.createElement('div');
    let errorElement = document.createElement('small');
    it("Passing in a bad year should be false", function() {
        expect(dateTest(badDataElement,parentElement,errorElement)).toEqual(false);
      });
    it("Passing in a good year should be true", function() {
        expect(dateTest(goodDataElement,parentElement,errorElement)).toEqual(true);
    });
});