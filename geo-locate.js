
let locationObject = {
    latitude: 0,
    longitude: 0,
    canLocate: true,
    hasBeenLocated: false
};

let MyGeoLocation = {
    
    setLocation: function(){
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                locationObject.latitude = position.coords.latitude;
                locationObject.longitude = position.coords.longitude;
                locationObject.canLocate = true;
                locationObject.hasBeenLocated = true;
                //this.setAttributes(position.coords.latitude, position.coords.longitude, true, true);
                let p = document.getElementById('lat-lon');
                p.innerHTML = `Current location is latitude ${locationObject.latitude} and longitude ${locationObject.longitude}`; 
                
                return true;
            });
        }else{
            this.canGeoLocate = false;
            return false;
        }
    },
    getLocateAttributes:function(){
        return{
            canGetLocation : locationObject.canLocate,
            hasBeenLocated: locationObject.hasBeenLocated
        };
    },
    getPosition:function(){
        return {
            latitude: locationObject.latitude,
            longitude: locationObject.longitude
        };
    }
    

};




