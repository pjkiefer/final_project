

const BASE_APOD_URL1 = 'https://api.nasa.gov/planetary/apod?date=';
const BASE_APOD_URL2 = `&api_key=${API_KEY}`;

const BASE_CURIOUSITY_MANIFEST_URL = `https://api.nasa.gov/mars-photos/api/v1/manifests/curiosity?api_key=${API_KEY}`;
const BASE_OPPORTUNITY_MANIFEST_URL = `https://api.nasa.gov/mars-photos/api/v1/manifests/opportunity?api_key=${API_KEY}`;
const BASE_SPIRIT_MANIFEST_URL = `https://api.nasa.gov/mars-photos/api/v1/manifests/spirit?api_key=${API_KEY}`;

const BASE_IMAGE_LIBRARY_URL1 = 'https://images-api.nasa.gov/search?q=';
const BASE_IMAGE_LIBRARY_URL2 = '&media_type=image';

const BASE_ROVER_URL1 = 'https://api.nasa.gov/mars-photos/api/v1/rovers/';
const BASE_ROVER_URL2 = '/photos?earth_date=';
const BASE_ROVER_URL3 = `&api_key=${API_KEY}`;

let buildSearchUrl = function(searchType, searchDate, searchTerm, roverName){
    let searchUrl = '';
    if(searchType === 'apod'){
        searchUrl = `${BASE_APOD_URL1}${searchDate}${BASE_APOD_URL2}`;
    }else if(searchType === 'ivl'){
        let encodedTerm = encodeURI(searchTerm);
        searchUrl = `${BASE_IMAGE_LIBRARY_URL1}${encodedTerm}${BASE_IMAGE_LIBRARY_URL2}`;
    }else if(searchType === 'mars-rovers'){
        if(roverName === 'curiousity'){
            roverName = 'curiosity'; //the url has this spelling
        }
        searchUrl = `${BASE_ROVER_URL1}${roverName}${BASE_ROVER_URL2}${searchDate}${BASE_ROVER_URL3}`;
    }
    return searchUrl;
};

let getRoverManifest = function(roverName){
    
    if(roverName.toLowerCase() === 'curiousity'){
        url = BASE_CURIOUSITY_MANIFEST_URL;
    }else if(roverName.toLowerCase() === 'opportunity'){
        url = BASE_OPPORTUNITY_MANIFEST_URL;
    }else if(roverName.toLowerCase() === 'spirit'){
       url = BASE_SPIRIT_MANIFEST_URL;
    }else{
        alert('No rover found');
        return;
    }
    
    fetch(url)
    .then((res) => res.json())
    .then((data)=> { 
        if(data === null){
            return;
        }
        let parentDiv = document.getElementsByClassName(`rover-${roverName.toLowerCase()}`)[0];
        let landingDate = parentDiv.getElementsByClassName('landing-date')[0];
        landingDate.innerHTML = '<span>Landing Date:</span>  ' + data.photo_manifest.landing_date;
        let launchDate = parentDiv.getElementsByClassName('launch-date')[0];
        launchDate.innerHTML = '<span>Launch Date:</span> ' + data.photo_manifest.launch_date;
        let maxDate = parentDiv.getElementsByClassName('max-date')[0];
        maxDate.innerHTML = '<span>Last Active Date:</span> ' + data.photo_manifest.max_date;
        let status = parentDiv.getElementsByClassName('status')[0];
        status.innerHTML = '<span>Current Status:</span> ' + data.photo_manifest.status;
    })
    .catch((error) => console.log(error));
    
};
let getImageJson = function(url, type){
    fetch(url)
    .then((res) => res.json())
    .then((data)=> { 
        if(data === null){
            alert('Whoops there was an error getting the data from NASA');
            return;
        }
        if(type === 'apod'){
            processApodImages(data);
        }else if(type === 'ivl'){
            processIvlImages(data);
        }else if(type === 'mars-rovers'){
            processRoverImages(data);
        }
       
    })
    .catch((error) => console.log(error));
};

