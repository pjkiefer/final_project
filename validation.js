let dataValidation = function(){

    let dateElement = document.getElementsByName('search-date')[0];
    let dateParent = dateElement.parentNode;
    let dateError = dateParent.getElementsByClassName('error')[0];

    let termElement = document.getElementsByName('search-term')[0];
    let termParent = termElement.parentNode;
    let termError = termParent.getElementsByClassName('error')[0];

    let roverParent = marsRoverType.parentNode;
    let roverError = roverParent.getElementsByClassName('error')[0];

    let imageTypeParent = imageType.parentNode;
    let imageTypeError = imageTypeParent.getElementsByClassName('error')[0];

    let passValidation = true;
    if(imageType.value === ''){
        imageTypeParent.classList.add('invalid');
        imageTypeError.textContent = 'Please select an item to search images for';
        return false;
    }else{
        imageTypeError.textContent = '';
        imageTypeParent.classList.remove('invalid');
    }
    let dateTestResult = true;
    let termTestResult = true;
    let roverTestResult = true;
    if(imageType.value === 'apod'){
        dateTestResult = dateTest(dateElement, dateParent, dateError);
    }else if(imageType.value === 'ivl'){
        termTestResult = termTest(termElement, termParent, termError);
    }else if(imageType.value === 'mars-rovers'){
        roverTestResult = roverTest(marsRoverType, roverParent, roverError);
        dateTestResult = dateTest(dateElement, dateParent, dateError);
    }
    if(dateTestResult === false || termTestResult === false || roverTestResult === false){
        passValidation = false;
    }
    return passValidation;
};

let dateTest = function(dateElement, dateParent, dateError){
    var dateRegex = /\d{4}-\d{2}-\d{2}/;
    var result = dateRegex.test(dateElement.value);
    if(result === false){
        dateParent.classList.add('invalid');
        dateError.textContent = 'Please enter a date in the format 2019-03-25';
        return false;
    }
    else{
        dateError.textContent = '';
        dateParent.classList.remove('invalid');
        return true;
    }
};

let termTest = function(termElement, termParent, termError){
    if(termElement.value){
        termError.textContent = '';
        termParent.classList.remove('invalid');
        return true;  
    }else{
        termParent.classList.add('invalid');
        termError.textContent = 'Please enter a search term';
        return false;
    }
};
let roverTest = function(marsRoverType, roverParent, roverError){
    if(marsRoverType.value){
        roverError.textContent = '';
        roverParent.classList.remove('invalid');
        return true;
    }else{
        roverParent.classList.add('invalid');
        roverError.textContent = 'Please select a mars rover';
        return false;
    }
};