let selectedImageDiv;

let imageResize = function(width, height){
    if(width <= 600){
        return {
            newWidth: width,
            newHeight: height
        };
    }
    let modifier = width / 600;
    let modifiedHeight = height / modifier;
    return{
        newWidth: 600,
        newHeight: Math.round(modifiedHeight)
    };
};

let displayNasaImage = function(){
    let oldWidth = imgTag.width;
    let oldHeight = imgTag.height;
    let newInfo = imageResize(oldWidth, oldHeight);
    imgTag.width = newInfo.newWidth;
    imgTag.height = newInfo.newHeight;
};



let processRoverImages = function(data){
    imgTag.src='';
    clearImageDescription();
    hideImageDisplayDiv();
    clearImageListDiv();
    let count = data.photos.length;
    if(count === 0){
        alert('There are not any photos for that rover / date');
        return;
    }
    let counter = count > 100 ? 100 : count;
    for(let item = 0; item < counter; item++){
        let div = document.createElement('div');
        div.classList.add('border-shadow');
        let desc = document.createElement('p');
        let hrf = document.createElement('p');
        desc.innerHTML = data.photos[item].camera.full_name;
        hrf.innerText = data.photos[item].img_src;
        hrf.classList.add('href-link');
        div.appendChild(desc);
        div.appendChild(hrf);
        imageListDiv.appendChild(div);
    }
    showImageListDiv();
};

let processApodImages = function(data){
    imgTag.src='';
    hideImageDisplayDiv();
    clearImgAttributes();
    hideImageListDiv();
    clearImageListDiv();
    imgTag.src = data.url;
    imageDescriptionP.innerHTML = data.explanation;
    showImageDisplayDiv();
};

let processIvlImages = function(data){
    clearImageDescription();
    imgTag.src='';
    hideImageDisplayDiv();
    hideImageListDiv();
    clearImageListDiv();
    let count = data.collection.metadata.total_hits;
    if(count === 0){
        alert('There are not any photos for that search criteria');
        return;
    }
    let counter = count > 100 ? 100 : count;
    for(let item = 0; item < counter; item++){
        let div = document.createElement('div');
        div.classList.add('border-shadow');
        let desc = document.createElement('p');
        let hrf = document.createElement('p');
        desc.innerHTML = data.collection.items[item].data[0].description;
        hrf.innerText = data.collection.items[item].links[0].href;
        hrf.classList.add('href-link');
        div.appendChild(desc);
        div.appendChild(hrf);
        imageListDiv.appendChild(div);
    }
    showImageListDiv();
};

let clearImageDescription = function(){
    let imageDescription = document.getElementById('imageDescription');
    imageDescription.innerHTML = '';
};
let hideImageTag = function(){
    imgTag.classList.add('displayHidden');
};

let showImageTag = function(){
    imgTag.classList.remove('displayHidden');
};

let hideImageListDiv = function(){
    imageListDiv.classList.add('displayHidden');
};

let showImageListDiv = function(){
    imageListDiv.classList.remove('displayHidden');
};

let hideImageDisplayDiv = function(){
    imageDisplayDiv.classList.add('displayHidden');
};

let showImageDisplayDiv = function(){
    imageDisplayDiv.classList.remove('displayHidden');
};

let clearImageListDiv = function(){
    while(imageListDiv.firstChild){
        imageListDiv.removeChild(imageListDiv.firstChild);
    }
};

let clearImgAttributes = function(){
    imgTag.removeAttribute('width');
    imgTag.removeAttribute('height');
};

function imageListDivClick(event){
    let element = event.target;
    if(element.id === 'imageList'){
        return;
    }

    let parent = element.parentNode;
    let href = parent.getElementsByClassName('href-link')[0];
    if(selectedImageDiv){
        selectedImageDiv.classList.remove('border-shadow-selected');
        selectedImageDiv.classList.add('border-shadow');
    }
    parent.classList.remove('border-shadow');
    parent.classList.add('border-shadow-selected');
    selectedImageDiv = parent;
    imgTag.src='';
    imgTag.src = href.innerText;
    showImageDisplayDiv();
}
