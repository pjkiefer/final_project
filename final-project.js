window.onload = function(){
    marsRoverType = document.getElementById('rover-search-criteria');
    imageType = document.getElementById("image-search-criteria");
    
    imageDisplayDiv = document.getElementById('imageDisplay');
    imageDescriptionP = document.getElementById('imageDescription');
    imageType.onchange = function() {
        let roverSearchDiv = document.getElementsByClassName('rover-search')[0];
        let dateFieldDiv = document.getElementsByClassName('date-field')[0];
        let searchTermDiv = document.getElementsByClassName('search-term')[0];
        let roverCuriousityDiv = document.getElementsByClassName('rover-curiousity')[0];
        let roverOpportunityDiv = document.getElementsByClassName('rover-opportunity')[0];
        let roverSpiritDiv = document.getElementsByClassName('rover-spirit')[0];
        if(imageType.value === 'mars-rovers'){
            dateFieldDiv.classList.remove('displayHidden');
            searchTermDiv.classList.add('displayHidden');
            roverSearchDiv.classList.remove('displayHidden');
            let event = new Event('change');
            marsRoverType.dispatchEvent(event);
        }else if(imageType.value === 'apod')
        {
            
            dateFieldDiv.classList.remove('displayHidden');
            roverSearchDiv.classList.add('displayHidden');
            searchTermDiv.classList.add('displayHidden');
            roverCuriousityDiv.classList.add('displayHidden');
            roverOpportunityDiv.classList.add('displayHidden');
            roverSpiritDiv.classList.add('displayHidden');
        }else if(imageType.value === 'ivl')
        {
            dateFieldDiv.classList.add('displayHidden');
            roverSearchDiv.classList.add('displayHidden');
            searchTermDiv.classList.remove('displayHidden');
            roverCuriousityDiv.classList.add('displayHidden');
            roverOpportunityDiv.classList.add('displayHidden');
            roverSpiritDiv.classList.add('displayHidden');
        }
    };
    marsRoverType.onchange = function(){
        let roverCuriousityDiv = document.getElementsByClassName('rover-curiousity')[0];
        let roverOpportunityDiv = document.getElementsByClassName('rover-opportunity')[0];
        let roverSpiritDiv = document.getElementsByClassName('rover-spirit')[0];
        if(marsRoverType.value === 'curiousity'){
            roverCuriousityDiv.classList.remove('displayHidden');
            roverOpportunityDiv.classList.add('displayHidden');
            roverSpiritDiv.classList.add('displayHidden');
            
        }else if(marsRoverType.value === 'opportunity')
        {
            roverCuriousityDiv.classList.add('displayHidden');
            roverOpportunityDiv.classList.remove('displayHidden');
            roverSpiritDiv.classList.add('displayHidden');
            
        }else if(marsRoverType.value === 'spirit')
        {
            roverCuriousityDiv.classList.add('displayHidden');
            roverOpportunityDiv.classList.add('displayHidden');
            roverSpiritDiv.classList.remove('displayHidden');
            
        }
    };

    form = document.getElementsByTagName('form')[0];
    form.addEventListener("submit", processSubmit, false);

    imgTag = document.getElementById('theImage');
    imgTag.addEventListener('load',displayNasaImage, false);
    imageListDiv = document.getElementById('imageList');
    imageListDiv.addEventListener('click', imageListDivClick, false);

    getRoverManifests();

};

/*is fired on window.load and loads the manifest information for all the rovers*/
let getRoverManifests = function(){
    getRoverManifest('opportunity');
    getRoverManifest('spirit');
    getRoverManifest('curiousity');
};

/*global variables*/
let myLocation = MyGeoLocation; // not currently used
let searchRequest;
let imageType; //dropdown where the user picks which image type to look for
let imgTag; //the img tag where downloaded image is displayed
let marsRoverType; //dropdown where the user chooses which mars rover to search for pics for
let form; // the page form element
let imageListDiv; // the div which displays a list of images available pulled from the NASA JSON
let imageDisplayDiv; // the div element which contains the img tag which displays the image
let imageDescriptionP;

/*processes when the submit button is pressed*/ 
let processSubmit = function(event){
    let validateResult = dataValidation();
    if(validateResult === true){
        let dateElement = document.getElementsByName('search-date')[0];
        let termElement = document.getElementsByName('search-term')[0];
        
        let searchUrl = buildSearchUrl(imageType.value, dateElement.value, termElement.value, marsRoverType.value);
        getImageJson(searchUrl, imageType.value);
        document.cookie = `searchUrl=${searchUrl}`;
    }
    event.preventDefault();
};





